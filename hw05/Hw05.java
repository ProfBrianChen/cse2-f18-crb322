// Christian Blake, 10/08/18, CSE 002
//
//
import java.util.Scanner; // importants Scanner class
import java.text.DecimalFormat; // imports DecimalFormat class

public class Hw05{
  public static void main (String [] args){ // start of main method
    Scanner myScanner = new Scanner( System.in ); // creates a scanner object to gather text input from the user
    
    DecimalFormat df = new DecimalFormat("###.###"); // creates a DecimalFormat object to limit double values to a max of three decimal places
    
    // used different decimal format object because probability of four-of-a-kind not show if limited to 3 decimals
    DecimalFormat ef = new DecimalFormat("###.#####"); // creates a DecimalFormat object to limit double values to a max of five decimal places
    int handCount = 0; // declares variable for amount of loops going to be generated
    boolean flag  = false; // creates a boolean to make sure user enters integer
    System.out.println("How many hands would you like to generate?: ");
    while(flag == false){
      boolean correct = myScanner.hasNextInt(); // checks if user entered value is an int
      if (correct){ // if int, then store in handCount
    	  handCount = myScanner.nextInt();
        flag = true; // resets flag value
      }
      else{
        System.out.println("Please enter a valid integer: "); // prompts user to enter a valid int
        myScanner.next(); // throws away improper value
      }
    }
    System.out.println(""); // blank line to keep output neat
    
    int fourOfKind = 0; // creates int counter variables
    int threeOfKind = 0;
    int twoPair = 0;
    int onePair = 0;
    
    int cardOne = 0; // declares card values that reset each time new hand is generated
    int cardTwo = 0;
    int cardThree = 0;
    int cardFour = 0;
    int cardFive = 0;
    int counter = handCount; // stores amount of loops in a counter that will be decreased by 1 every iteration of loop
    
    while (counter > 0) { // repeat loop until counter is 0
    	cardOne = (int)(Math.random()*52); // creates random number between 0-51
    	
    	cardTwo = (int)(Math.random()*52);
    	while (cardTwo == cardOne) { // makes sure two card values are not the same
    		cardTwo = (int)(Math.random()*52);
    	}
    	
    	cardThree = (int)(Math.random()*52);
    	while (cardThree == cardOne || cardThree == cardTwo) {
    		cardThree = (int)(Math.random()*52);
    	}
    	
    	cardFour = (int)(Math.random()*52);
    	while (cardFour == cardOne || cardFour == cardTwo || cardFour == cardThree) { // makes sure two card values are not the same
    		cardFour = (int)(Math.random()*52);
    	}
    	
    	cardFive = (int)(Math.random()*52);
    	while (cardFive == cardOne || cardFive == cardTwo || cardFive == cardThree || cardFive == cardFour) { // makes sure two card values are not the same
    		cardFive = (int)(Math.random()*52);
    	}
    	
    	// lowers all cards to their face value, no more need for suits
    	cardOne = cardOne % 13; 
    	cardTwo = cardTwo % 13;
    	cardThree = cardThree % 13;
    	cardFour = cardFour % 13;
    	cardFive = cardFive % 13;
    	
    	// checks for every possibility for a four-of-a-kind
    	if((cardOne == cardTwo && cardOne == cardThree && cardOne == cardFour)
    		||(cardTwo == cardThree && cardTwo == cardFour && cardTwo == cardFive)
    		||(cardThree == cardFour && cardThree == cardFive && cardThree == cardOne)
    		||(cardFour == cardFive && cardFour == cardOne && cardFour == cardTwo)
    		||(cardFive == cardOne && cardFive == cardTwo && cardFive == cardThree)) {
    		fourOfKind++; // adds 1 to the four-of-a-kind counter
    	}
    	
    	// checks for every possibility for a three-of-a-kind
    	else if((cardOne == cardTwo && cardOne == cardThree)
    			||(cardOne == cardTwo && cardOne == cardFour)
    			||(cardOne == cardTwo && cardOne == cardFive)
    			||(cardOne == cardThree && cardOne == cardFour)
    			||(cardOne == cardThree && cardOne == cardFive)
    			||(cardOne == cardFour && cardOne == cardFive)
    			||(cardTwo == cardThree && cardTwo == cardFour)
    			||(cardTwo == cardFour && cardTwo == cardFive)
    			||(cardThree == cardFour && cardThree == cardFive)) {
    		if((cardOne == cardTwo)||(cardOne == cardThree)||(cardOne == cardFour)||(cardOne == cardFive) // checks if there is a One pair along with the three of a kind
    			||(cardTwo == cardThree)||(cardTwo == cardFour)||(cardTwo == cardFive)||(cardThree == cardFour)
    			||(cardThree == cardFive)||(cardFour == cardFive)) {
    			onePair++;
    		}
    		threeOfKind++;
    	}
    	
    	// checks for every possibility for a Two-pair
    	else if((cardOne == cardTwo && cardThree == cardFour)
    			|| (cardOne == cardTwo && cardFour == cardFive)
    			|| (cardOne == cardThree && cardTwo == cardFour)
    			|| (cardOne == cardThree && cardTwo == cardFive)
    			|| (cardOne == cardThree && cardFour == cardFive)
    			|| (cardOne == cardFour && cardTwo == cardThree)
    			|| (cardOne == cardFour && cardTwo == cardFive)
    			|| (cardOne == cardFour && cardThree == cardFive)
    			|| (cardOne == cardFive && cardTwo == cardThree)
    			|| (cardOne == cardFive && cardTwo == cardFour)
    			|| (cardOne == cardFive && cardThree == cardFour)
    			|| (cardTwo == cardThree && cardFour == cardFive)
    			|| (cardTwo == cardFour && cardThree == cardFive)
    			|| (cardTwo == cardFive && cardThree == cardFour)){
    		twoPair++;
    	}
    	// checks for every possibility for a One-pair
    	else if((cardOne == cardTwo)||(cardOne == cardThree)||(cardOne == cardFour)||(cardOne == cardFive)
    			||(cardTwo == cardThree)||(cardTwo == cardFour)||(cardTwo == cardFive)||(cardThree == cardFour)
    			||(cardThree == cardFive)||(cardFour == cardFive)) {
    			onePair++;
    		}
    	
    	
    	counter--; // lowers counter by 1
    }
    
    //prints out all required information
    System.out.println("Number of loops: " + handCount);
    System.out.println("Probability of Four-of-a-kind: " + (ef.format((double)fourOfKind / handCount)));
    System.out.println("Probability of Three-of-a-kind: " + (df.format((double)threeOfKind / handCount)));
    System.out.println("Probability of Two-pair: " + (df.format((double)twoPair / handCount)));
    System.out.println("Probability of One-pair: " + (df.format((double)onePair / handCount)));
}
}