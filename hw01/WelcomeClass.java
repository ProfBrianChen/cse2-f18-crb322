public class WelcomeClass{
  public static void main (String args[]){
    System.out.println("-----------");
    System.out.println("| WELCOME |");
    System.out.println("-----------");
    System.out.println(" ^  ^  ^  ^  ^  ^");
    System.out.println("/ \\/ \\/ \\/ \\/ \\/ \\ ");
    System.out.println("<-C--R--B--3--2--2->");
    System.out.println("\\ /\\ /\\ /\\ /\\ /\\ /");
    System.out.println(" v  v  v  v  v  v ");
    System.out.println("When I was 12 years old I had a kite. It blew away in the wind, and I was sad.");
  }
}