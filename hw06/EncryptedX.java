// Christian Blake, 10/22/18, CSE 002
//
//
import java.util.Scanner; // imports Scanner class

public class EncryptedX {
	public static void main (String [] args) {
		Scanner myScanner = new Scanner( System.in ); // creates a scanner object to gather text input from the user
		System.out.println("Please print an integer between 0-100"); 
		
		boolean flag = false; // flag to end loop
		int xSize = 0; // size of printed image, size 10 will print an output of characters that are 11x11 (to match given example)
		while(flag == false){ // loop will continue until user inputs integer between 0 and 100
			boolean correct = myScanner.hasNextInt();
			if (correct){ // if int, 
		    	  xSize = myScanner.nextInt();
		    	  if (xSize >=0 && xSize <= 100) {
		    		  flag = true; // resets flag value
		    	  }
		    	  else {
		    		  System.out.println("Please enter a valid integer: "); // prompts user to enter a valid int
				      myScanner.next(); // throws away improper value
		    	  }
		      }
		      else {
		        System.out.println("Please enter a valid integer: "); // prompts user to enter a valid int
		        myScanner.next(); // throws away improper value
		      }
		    }
		
		for (int r = 1; r <= xSize+1; r++) { // for loop of rows and columns
			for (int c = 1; c <= xSize+1; c++) {
				int secondSpace = xSize + 2 - r; // declares the second location for the space (the first is the row #)
				if (c == r || c == secondSpace) { // line 34-40 choose whether to print out a space or a *
					System.out.print(" ");
				}
				
				else {
					System.out.print("*");
				}
			}
			System.out.println(""); // begins a new line in output
		}
	}
} // end of class
