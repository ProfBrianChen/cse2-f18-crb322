// Christian Blake, CSE 002, 09/10/18
//

import java.text.DecimalFormat; // imports DecimalFormat class

public class Arithmetic{
  public static void main (String args[]){
    //Number of pairs of pants
    int numPants = 3;
    //Cost per pair of pants
    double pantsPrice = 34.98;

    //Number of sweatshirts
    int numShirts = 2;
    //Cost per shirt
    double shirtPrice = 24.99;

    //Number of belts
    int numBelts = 1;
    //cost per belt
    double beltCost = 33.99;

    //the tax rate
    double paSalesTax = 0.06;

    double totalCostOfPants; // total cost of pants
    double totalCostOfShirts;  // total cost of sweatshirts
    double totalCostOfBelts;  // total cost of belts
    totalCostOfPants = pantsPrice * numPants;
    totalCostOfShirts = shirtPrice * numShirts;
    totalCostOfBelts = beltCost * numBelts;
    double taxForPants = totalCostOfPants * paSalesTax;
    double taxForShirts = totalCostOfShirts * paSalesTax;
    double taxForBelts = totalCostOfBelts * paSalesTax;
    
    // adds the previous values of costs and taxes together to simplify the print call
    double totalCost = totalCostOfPants + totalCostOfShirts + totalCostOfBelts;
    double totalTax = taxForPants + taxForShirts + taxForBelts;
    
    DecimalFormat df = new DecimalFormat("###.##"); // creates a DecimalFormat object to limit double values to a max of two decimal places
    
    // // prints the total price of the three items, along with the rounded tax values
    System.out.println("Pants total price: " + totalCostOfPants +
          ". Total tax: " + df.format(taxForPants)); 
    System.out.println("Shirts total price: " + totalCostOfShirts +
          ". Total tax: " + df.format(taxForShirts));
    System.out.println("Belts total price: " + totalCostOfBelts +
          ". Total tax: " + df.format(taxForBelts));
    
    System.out.println(""); // prints a blank line to organize output
    System.out.println("Total cost before tax: " + totalCost); // prints total cost of items before tax
    System.out.println("Total sales tax: " + df.format(totalTax)); // prints a rounded value of total sales tax
    System.out.println("Final cost: " + df.format(totalCost + totalTax)); // prints the total value of everything
  }
}
