import java.util.Scanner;

// Christian Blake. 09/13/18. CSE 002
//
//
public class Check{
  // main method required for every Java program
  public static void main(String[] args) {
    Scanner myScanner = new Scanner( System.in ); // creates a scanner object to gather text input from the user
    System.out.print("Enter the original cost of the check" // tells the user what to input
      + " in the form xx.xx: ");
                     
    double checkCost = myScanner.nextDouble(); // stores the value that the user inputed
    System.out.print("Enter the percentage tip that you" // tells the user what to input
     + " wish to pay as a whole number (in the form xx): "  );
    double tipPercent = myScanner.nextDouble(); // stores the value that the user inputed
    tipPercent /= 100; //We want to convert the percentage into a decimal value
    System.out.print("Enter the number of people who went" // tells the user what to input
      + " out to dinner: ");
                     
    int numPeople = myScanner.nextInt(); // stores the value that the user inputed
    double totalCost; // creates the variable for the total cost
    double costPerPerson; // createst the variable for the cost per person
    int dollars,   //whole dollar amount of cost 
      dimes, pennies; //for storing digits
                      //to the right of the decimal point 
                      //for the cost$ 
    
    // finds the total cost by adding the total check cost and tip
    totalCost = checkCost * (1 + tipPercent); 
    costPerPerson = totalCost / numPeople; // finds the cost per person by dividing the total cost by the amount of people
    //get the whole amount, dropping decimal fraction
    dollars=(int)costPerPerson;
    //get dimes amount, e.g., 
    // (int)(6.73 * 10) % 10 -> 67 % 10 -> 7
    //  where the % (mod) operator returns the remainder
    //  after the division:   583%100 -> 83, 27%5 -> 2 
    dimes=(int)(costPerPerson * 10) % 10;
    pennies=(int)(costPerPerson * 100) % 10;
    // prints the amount that each person in the group owes in dollars                
    System.out.println("Each person in the group owes $" + dollars + "." + dimes + pennies);
}  //end of main method   
  	} //end of class
