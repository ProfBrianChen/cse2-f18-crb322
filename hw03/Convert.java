// Christian Blake, 09/17/18, CSE 002
//
//
import java.util.Scanner;

public class Convert{
  public static void main(String args[]){
    Scanner myScanner = new Scanner( System.in ); // creates a scanner object to gather text input from the user
    
    System.out.print("Enter the affected area in acres: "); // tells the user what to input
    double acres = myScanner.nextDouble(); // stores the value that the user inputed
    System.out.print("Enter the rainfall in the affected area: "); // tells the user what to input
    double inchesOfRain = myScanner.nextDouble(); // stores the value that the user inputed
                      
    final double INCHES_PER_ACRE = 27154; // The amount of water in gallons of 1 inch of water over an acre
    double totalGallons = acres * inchesOfRain * INCHES_PER_ACRE; // calculates and stores the volume of the rain in gallons
    double gallonsPerMile = 1101117130711.3; // stores the amount of gallons per 1 cubic mile in a double
    double totalCubicMiles = totalGallons / gallonsPerMile; // calculates and stores the volume of rainfall in cubic miles
                     
    System.out.println(totalCubicMiles + " cubic miles"); // prints out the volume of rainfall in cubic miles
    
  } // end of main method
} // end of class