// Christian Blake, 09/17/18, CSE 002
//
//
import java.util.Scanner;

public class Pyramid{
  public static void main (String args[]){
    Scanner myScanner = new Scanner( System.in ); // creates a scanner object to gather text input from the user
    
    System.out.print("Enter the length of the square side of the pyramid: "); // tells the user what to input
    double length = myScanner.nextDouble(); // stores the value that the user inputed
    System.out.print("Enter the height of the pyramid: "); // tells the user what to input
    double height = myScanner.nextDouble(); // stores the value that the user inputed
    
    double volume = (length * length * height) / 3; // calculates and stores the volume of the pyramid
    
    System.out.println("The volume inside the pyramid is: " + volume); // prints out the volume of the pyramid
  } // end of main method
} // end of class