import java.util.Scanner;
import java.util.Random;
public class hw08{
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in); 
		//suits club, heart, spade or diamond 
		String[] suitNames={"C","H","S","D"};    
		String[] rankNames={"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q","K","A"}; 
		String[] cards = new String[52]; 
		String[] hand = new String[5]; 
		int numCards = 5; 
		int again = 1; 
		int index = 51;
		for (int i=0; i<52; i++){ 
			cards[i]=rankNames[i%13]+suitNames[i/13];
		}
		printArray(cards);
		System.out.println();
		shuffle(cards); 
		printArray(cards);
		System.out.println();
		System.out.println("Shuffled");
		while(again == 1){
			if (index - numCards >=0){
				hand = getHand(cards,index,numCards);
				System.out.print("Hand: ");
				printArray(hand);
				System.out.println();
				index = index - numCards;
				System.out.println("Enter a 1 if you want another hand drawn"); 
				again = scan.nextInt();
			}
			else{
				cards = new String[52];
				for (int j=0; j<52; j++){ 
					cards[j]=rankNames[j%13]+suitNames[j/13];
				}
				shuffle(cards);
				System.out.println("\nNew deck created.\n");
				index = 51;
				hand = getHand(cards,index,numCards);
			}
			
		}
	}
	
	public static void printArray (String [] list){
		for (int i = 0; i<list.length; i++){
			System.out.print(list[i] + " ");
		}
	}
	
	public static void shuffle (String [] list){
		for (int i = 1; i<100; i++){
			int randomIndex = new Random().nextInt(list.length);
			if (randomIndex != 0){
				String temp = list[randomIndex];
				list [randomIndex] = list[0];
				list[0] = temp;
			}
		}
	}
	
	public static String [] getHand (String [] list, int index, int numCards){
		String [] hand = new String [numCards];
		int handIndex = 0;
		for (int i = index; i>=index-numCards+1; i--){
			hand[handIndex] = list[i];
			handIndex++;
		}
		return hand;
	}
}
