//Christian Blake, CSE 002, 10/30/18
//
//
import java.util.Scanner; // imports Scanner class

public class WordTools {
	private static String origText;
	public static void main (String [] args) {
		origText = "";
		Scanner myScanner = new Scanner( System.in ); // creates a scanner object to gather text input from the user
		System.out.println("Please enter a sample text: ");
		origText = myScanner.nextLine();
		System.out.println("You entered: " + sampleText());
		char answer = printMenu();
		while( answer != 'q') {
			if (answer == 'c') {
				System.out.println(getNumOfNonWSCharacters(sampleText()) + " instances.");
			}
			if (answer == 'w') {
				System.out.println(getNumOfWords(sampleText()) + " instances.");
			}
			if (answer == 'f') {
				System.out.println("Please enter a word to be found: ");
				String word = myScanner.nextLine();
				System.out.println(findText(word, sampleText()) + " instances.");
			}
			if (answer == 'r') {
				System.out.println(replaceExclamation(sampleText()));
			}
			if (answer == 's') {
				System.out.println(shortenSpace(sampleText()));
				answer = ' ';
			}
			else {
				answer = printMenu();
			}
		}
	}
	
	public static String sampleText() {
		String text = origText;
		return text;
	}
	
	public static char printMenu() {
		Scanner myScanner = new Scanner( System.in ); // creates a scanner object to gather text input from the user
		char userChoice = ' ';
		System.out.println("");
		System.out.println("MENU");
		System.out.println("c - Number of non-whitespace characters");
		System.out.println("w - Number of words");
		System.out.println("f - Find text");
		System.out.println("r - Replace all !'s");
		System.out.println("s - Shorten spaces");
		System.out.println("q - Quit"); 
		System.out.println("");
		System.out.println("Choose an option:");
		userChoice = myScanner.next().charAt(0);
		
		return userChoice;
	}

	public static int getNumOfNonWSCharacters(String str) {
		int numOfChars = 0;
		int length = str.length()-1;
		for (int i = 0; i <= length; i++) {
			if (str.charAt(i)!=' ') {
				numOfChars++;
			}
		}
		return numOfChars;
	}
	
	public static int getNumOfWords(String str) {
		int numOfWords = 0;
		int length = str.length()-1;
		for (int i = 0; i < length; i++) {
			if (str.charAt(i) != ' ') {
				if (str.charAt(i+1) == ' ') {
					numOfWords++;
				}
			}
		}
		return numOfWords;
	}
	
	public static int findText(String text, String paragraph) {
		int foundText = 0;
		foundText = paragraph.split(text, -1).length-1;
		
		return foundText;
	}
	
	public static String replaceExclamation(String str) {
		String editedText = str.replace('!', '.');
		
		return editedText;
	}
	
	public static String shortenSpace(String str) {
		String editedText = str.replaceAll("\\s+", " ");
		
		return editedText;
	}
}
