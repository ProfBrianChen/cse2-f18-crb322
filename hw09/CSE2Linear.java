import java.util.Random;
import java.util.Scanner;

public class CSE2Linear {
	public static void main (String [] args){
		Scanner myScanner = new Scanner(System.in);
		int [] grades = new int [15];
		System.out.println("Enter 15 ascending ints for final grades in CSE2: ");
		for (int i = 0; i <15; i++){
			boolean flag = false; // flag to end loop
			while(flag == false){ // loop will continue until user inputs integer between 0 and 100
				boolean correct = myScanner.hasNextInt();
				if (correct){ // if int,
					grades[i] = myScanner.nextInt();
			    	if (grades[i] >=0 && grades[i] <= 100) {
			    		  if (i == 0){
				    		  flag = true; // resets flag value
			    		  }
			    		  else if (grades[i] >= grades[i-1]){
				    		  flag = true; // resets flag value
			    		  }
			    		  else{
			    			  System.out.println("Values must be in ascending order: ");
			    			 // myScanner.next();
			    		  }
			    	  }
			    	  else {
			    		  System.out.println("Please enter an int from 0-100: "); // prompts user to enter a valid int
					      myScanner.next(); // throws away improper value
			    	  }
			      }
			      else {
			        System.out.println("Please enter an integer: "); // prompts user to enter a valid int
			        myScanner.next(); // throws away improper value
			      }
			    }
		}
		printArray(grades);
		System.out.println("Enter a grade to search for: ");
		int gradeToFind = myScanner.nextInt();
		binarySearch(grades, gradeToFind);
		scramble(grades);
		printArray(grades);
		System.out.println("Enter a grade to search for: ");
		gradeToFind = myScanner.nextInt();
		linearSearch(grades, gradeToFind);
	}

	public static void linearSearch(int [] grades, int grade){
		int iterations = 0;
		for (int i = 0; i<grades.length; i++){
			if (grades[i] == grade){
				System.out.println(grade + " was found in the list with " + iterations + " iterations" );
				return;
			}
			else{
				iterations++;
			}
		}
		System.out.println(grade + " was not found in the list with " + iterations + " iterations" );
	}
	// credit to Professor Carr for this binary search method
	public static void binarySearch(int [] grades, int grade){
		int low = 0;
		int high = grades.length-1;
		int iterations = 0;
		while (high >= low){
			int mid = (low +high)/2;
			if (grade < grades[mid]){
				high = mid - 1;
				iterations++;
			}
			else if (grade == grades[mid]){	
				System.out.println(grade + " was found in the list with " + iterations + " iterations" );
				return;
			}
			else{
				low = mid + 1;
				iterations++;
			}
		}
		System.out.println(grade + " was not found in the list with " + iterations + " iterations" );
	}
	
	public static void scramble (int [] array){
		for (int i = 0; i<100; i++){
			int randomIndex = new Random().nextInt(array.length);
			if (randomIndex != 0){
				int temp = array[randomIndex];
				array [randomIndex] = array[0];
				array[0] = temp;
			}
		}
	}
	
	public static void printArray(int [] array){
		for (int i = 0; i<array.length; i++){
			System.out.print(array[i] + " ");
		}
		System.out.println();
	}
}
