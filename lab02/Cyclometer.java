// Christian Blake, 09/06/18, CSE 002
//The program cyclometer records data from trips taken on a bicycle, specifically the # of minutes, # of counts,
  //distance in miles, and distance of two trips combined
public class Cyclometer{
  public static void main (String args[]){
    int secsTrip1=480;  // The amount of seconds that the first trip took
    int secsTrip2=3220;  // The amount of seconds that the second trip took
		int countsTrip1=1561;  // The amount of wheel rotations during the first trip
		int countsTrip2=9037; // The amount of wheel rotations during the second trip
    
    double wheelDiameter=27.0,  // The diameter of the bike wheel in inches
  	PI=3.14159, // The numeric value of pi
  	feetPerMile=5280,  // The amount of feet in 1 mile
  	inchesPerFoot=12,   // The amount of inches in 1 foot
  	secondsPerMinute=60;  // The amount of seconds in 1 minute
	  double distanceTrip1, distanceTrip2,totalDistance;  // Creates the valiables which will later store trip distances in miles
    
    
     System.out.println("Trip 1 took "+
       	     (secsTrip1/secondsPerMinute)+" minutes and had "+
       	      countsTrip1+" counts.");  // Prints out how many minutes trip 1 took and how many wheel roations there were
	       System.out.println("Trip 2 took "+
       	     (secsTrip2/secondsPerMinute)+" minutes and had "+
       	      countsTrip2+" counts.");  // Prints out how many minutes trip 1 took and how many wheel roations there were
    
	distanceTrip1=countsTrip1*wheelDiameter*PI;
    	// Above gives distance in inches
    	//(for each count, a rotation of the wheel travels
    	//the diameter in inches times PI)
	distanceTrip1/=inchesPerFoot*feetPerMile; // Gives distance in miles
	distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile; // Gives distance in miles
	totalDistance=distanceTrip1+distanceTrip2; // Stores the combined distance in miles of trip 1 and trip 2
    
  //Print out the output data.
  System.out.println("Trip 1 was "+distanceTrip1+" miles");
	System.out.println("Trip 2 was "+distanceTrip2+" miles");
	System.out.println("The total distance was "+totalDistance+" miles");
  }
}