// Christian Blake, 09/25/18, CSE 002
// NOTE: Any code that has no comment was left blank because it was explained previously.
//
import java.util.Scanner;

public class CrapsIf{
	private static int firstInput = 0; // creates static variables to be used in main method and slang() method, same for next line
	private static int secondInput = 0;
  
  public static void main (String [] args){ // start of main method
    Scanner myScanner = new Scanner( System.in ); // creates a scanner object to gather text input from the user
    
    System.out.print("Enter 0 to generate random dice, or enter 1 to input your own values: "); // tells the user what value to input
    int randomOrNot = myScanner.nextInt(); // stores the user-entered value in an int
    
    if (randomOrNot == 0){ // happens if user chooses to have randomly generated dice
       firstInput = (int)(Math.random()*6) + 1; // sets one dice as a random number between 1-6, same for next line
       secondInput = (int)(Math.random()*6) + 1;
       System.out.println("Your dice are " + firstInput + " and " + secondInput); // prints out the dice rolls
       System.out.println("You rolled a " + slang()); // prints out the slang for the roll
    }
  
    
    else if (randomOrNot == 1){ // happens if user chooses to input own dice values
        System.out.print("Enter first dice value: "); // tells the user what value to input
        firstInput = myScanner.nextInt(); // stores the user-entered value in an int
        if (firstInput > 0 && firstInput <= 6){ // determines if first value is in-bounds (between 1-6 inclusive)
            System.out.print("Enter second dice value: ");
             secondInput = myScanner.nextInt();
            if (secondInput > 0 && secondInput <= 6){ // determines if second value is in-bounds (between 1-6 inclusive)
            	System.out.println("You rolled a " + slang()); // prints out the slang for the roll
            }
            
            else{
                System.out.println("Invalid input"); // prints if user enters number that is not between 1-6 inclusive
            }
        }
        else{
            System.out.println("Invalid input");
        }    
    }
    
    else{
        System.out.println("Invalid input"); // prints if user chooses option other than 0 or 1
  }
  }
  
  public static String slang(){ // this method takes the two dice rolls and determines the slang for them
      String result = ""; // declaration for returned variable
      int sum = firstInput + secondInput; // stores the sum of the two rolls
      if (sum == 2) { // if sum is 2, can only be snake eyes
    	  result = "Snake Eyes";
      }
      else if (sum == 3) {
    	  result = "Ace Deuce";
      }
      else if (sum == 4) { // if sume is 4, there is two possibilities
    	  if (firstInput == secondInput) { // determines if both inputs are the same. If not, goes to else statement
    		  result = "Hard four";
    	  }
    	  else {
    		  result = "Easy four";
    	  }
      }
      else if (sum == 5) {
    	  result = "Fever Five";
      }
      else if (sum == 6) {
    	  if (firstInput == secondInput) {
    		  result = "Hard six";
    	  }
    	  else {
    		  result = "Easy six";
    	  }
      }
      else if (sum == 7) {
    	  result = "Seven out";
      }
      else if (sum == 8) {
    	  if (firstInput == secondInput) {
    		  result = "Hard eight";
    	  }
    	  else {
    		  result = "Easy eight";
    	  }
      }
      else if (sum == 9) {
    	  result = "Nine";
      }
      else if (sum == 10) {
    	  if (firstInput == secondInput) {
    		  result = "Hard ten";
    	  }
    	  else {
    		  result = "Easy ten";
    	  }
      }
      else if (sum == 11) {
    	  result = "Yo-leven";
      }
      else if (sum == 12) {
    	  result = "Boxcars";
    	  }
     
      else {
    	  result = "Error"; // should not be possible
      }
      return result; // returns the result as a String
}
}