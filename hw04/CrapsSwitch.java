// Christian Blake, 09/25/18, CSE 002
// NOTE: Any code that has no comment was left blank because it was explained previously.
//
import java.util.Scanner;

public class CrapsSwitch{
	private static int firstInput = 0; // creates static variables to be used in main method and slang() method, same for next line
	private static int secondInput = 0;
  
  public static void main (String [] args){ // start of main method
    Scanner myScanner = new Scanner( System.in ); // creates a scanner object to gather text input from the user
    
    System.out.print("Enter 0 to generate random dice, or enter 1 to input your own values: "); // tells the user what value to input
    int randomOrNot = myScanner.nextInt(); // stores the user-entered value in an int
    
    switch ( randomOrNot ) { // outermost switch statement that separates random generated dice from user entered dice
    	case 0: // case 0 happens if user chooses to have randomly generated dice
    		firstInput = (int)(Math.random()*6) + 1; // sets one dice as a random number between 1-6, same for next line
    		secondInput = (int)(Math.random()*6) + 1;
        System.out.println("Your dice are " + firstInput + " and " + secondInput); // prints out the dice rolls
    		System.out.println("You rolled a " + slang()); // prints out the slang for the roll
    		break; // ends the switch statement
    	case 1:
    		System.out.print("Enter first dice value: "); // tells the user what value to input
    		firstInput = myScanner.nextInt(); // stores the user-entered value in an int
    		switch ( firstInput ){ /* these nested switch statements determine if both numbers are between 1-6, and 
                               only continue with program if both are in bounds */
    			case 1:              
    				System.out.print("Enter second dice value: "); // tells the user what value to input
    				secondInput = myScanner.nextInt(); // stores the user-entered value in an int
    				switch ( secondInput ) {
    					case 1:
    						System.out.println("You rolled a " + slang()); // prints out the slang for the roll
    						break;
    					case 2:
    						System.out.println("You rolled a " + slang());
    						break;
    					case 3:
    						System.out.println("You rolled a " + slang());
    						break;
    					case 4:
    						System.out.println("You rolled a " + slang());
    						break;
    					case 5:
    						System.out.println("You rolled a " + slang());
    						break;
    					case 6:
    						System.out.println("You rolled a " + slang());
    						break;
    					default:
    						System.out.println("Invalid Input"); // prints if user enters number that is not between 1-6 inclusive
    				}
            break;
    			case 2:
    				System.out.print("Enter second dice value: ");
    				secondInput = myScanner.nextInt();
    				switch ( secondInput ) {
    					case 1:
    						System.out.println("You rolled a " + slang());
    						break;
    					case 2:
    						System.out.println("You rolled a " + slang());
    						break;
    					case 3:
    						System.out.println("You rolled a " + slang());
    						break;
    					case 4:
    						System.out.println("You rolled a " + slang());
    						break;
    					case 5:
    						System.out.println("You rolled a " + slang());
    						break;
    					case 6:
    						System.out.println("You rolled a " + slang());
    						break;
    					default:
    						System.out.println("Invalid Input");
    				}
            break;
    			case 3:
    				System.out.print("Enter second dice value: ");
    				secondInput = myScanner.nextInt();
    				switch ( secondInput ) {
    					case 1:
    						System.out.println("You rolled a " + slang());
    						break;
    					case 2:
    						System.out.println("You rolled a " + slang());
    						break;
    					case 3:
    						System.out.println("You rolled a " + slang());
    						break;
    					case 4:
    						System.out.println("You rolled a " + slang());
    						break;
    					case 5:
    						System.out.println("You rolled a " + slang());
    						break;
    					case 6:
    						System.out.println("You rolled a " + slang());
    						break;
    					default:
    						System.out.println("Invalid Input");
    				}
            break;
    			case 4:
    				System.out.print("Enter second dice value: ");
    				secondInput = myScanner.nextInt();
    				switch ( secondInput ) {
    					case 1:
    						System.out.println("You rolled a " + slang());
    						break;
    					case 2:
    						System.out.println("You rolled a " + slang());
    						break;
    					case 3:
    						System.out.println("You rolled a " + slang());
    						break;
    					case 4:
    						System.out.println("You rolled a " + slang());
    						break;
    					case 5:
    						System.out.println("You rolled a " + slang());
    						break;
    					case 6:
    						System.out.println("You rolled a " + slang());
    						break;
    					default:
    						System.out.println("Invalid Input");
    				}
            break;
    			case 5:
    				System.out.print("Enter second dice value: ");
    				secondInput = myScanner.nextInt();
    				switch ( secondInput ) {
    					case 1:
    						System.out.println("You rolled a " + slang());
    						break;
    					case 2:
    						System.out.println("You rolled a " + slang());
    						break;
    					case 3:
    						System.out.println("You rolled a " + slang());
    						break;
    					case 4:
    						System.out.println("You rolled a " + slang());
    						break;
    					case 5:
    						System.out.println("You rolled a " + slang());
    						break;
    					case 6:
    						System.out.println("You rolled a " + slang());
    						break;
    					default:
    						System.out.println("Invalid Input");
    				}
            break;
    			case 6:
    				System.out.print("Enter second dice value: ");
    				secondInput = myScanner.nextInt();
    				switch ( secondInput ) {
    					case 1:
    						System.out.println("You rolled a " + slang());
    						break;
    					case 2:
    						System.out.println("You rolled a " + slang());
    						break;
    					case 3:
    						System.out.println("You rolled a " + slang());
    						break;
    					case 4:
    						System.out.println("You rolled a " + slang());
    						break;
    					case 5:
    						System.out.println("You rolled a " + slang());
    						break;
    					case 6:
    						System.out.println("You rolled a " + slang());
    						break;
    					default:
    						System.out.println("Invalid Input");
    				}
            break;
    		}
        break;
        
      default:
        System.out.println("Invalid Input"); // prints if user chooses option other than 0 or 1
    }
    }
    
  public static String slang(){ // this method takes the two dice rolls and determines the slang for them
      String result = ""; // declaration for returned variable
      int sum = firstInput + secondInput; // stores the sum of the two rolls
      switch ( sum ) {
      	  case 2: // if sum is 2, the result has to be snake eyes
      		  result = "Snake Eyes";
      		  break;
      	  case 3: 
      		  result = "Ace Deuce";
      		  break;
      	  case 4: 
      		  switch ( firstInput % 2 ){ // determines if two inputs are the same value (only can be even if both are the same)
      		  	  case 0:
      		  		  result = "Hard four";
      		  		  break;
      		  	  default: // if numbers are not same, this is only other possibility
      		  		  result = "Easy four";   
      		  }
            break;
      	  case 5:
      		  result = "Fever Five";
      		  break;
      	  case 6:
      		  switch ( firstInput % 3 ){
		  	      case 0:
		  		      result = "Hard six";
		  		      break;
		  	      default:
		  		      result = "Easy six";
		        } 
             break;
          case 7:
      		  result = "Seven out";
      		  break;
          case 8:
      		  switch ( firstInput % 4 ){
		  	      case 0:
		  		      result = "Hard eight";
		  		      break;
		  	      default:
		  		      result = "Easy eight";
		        }
            break;
          case 9:
      		  result = "Nine";
      		  break;
          case 10:
      		  switch ( firstInput % 5 ){
		  	      case 0:
		  		      result = "Hard ten";
		  		      break;
		  	      default:
		  		      result = "Easy ten";
		  }
            break;
          case 11:
      		  result = "Yo-leven";
      		  break;
          case 12:
      		  result = "Boxcars";
      		  break;
        default:
          result = "Error"; // should not be possible
      }
    return result; // returns the result as a String
}
}