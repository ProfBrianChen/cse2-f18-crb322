
public class Lab9 {
	public static void main (String [] args){
		int [] array0 = {0,1,2,3,4,5,6,7,8,9};
		
		int [] array1 = copy(array0);
		int [] array2 = copy(array0);
		inverter(array0);
		print(array0);
		inverter2(array1);
		print(array1);
		int [] array3 = inverter2(array2);
		print(array3);
	}

	public static int [] copy (int [] list){
		int [] copy = new int [list.length];
		for (int i = 0; i <copy.length; i++){
			copy[i] = list[i];
		}
		
		return list;
	}
	
	public static void inverter (int [] list){
		int length = list.length;
		for (int i = 0; i<length/2; i++){
			int temp = list[i];
			list[i] = list[length-1-i];
			list[length-1-i] = temp;
		}
	}
	
	public static int [] inverter2 (int [] list){
		int length = list.length;
		int [] invertedCopy = copy(list);
		inverter(invertedCopy);
		return invertedCopy;
	}
	
	public static void print (int [] list){
		for (int i = 0; i<list.length; i++){
			System.out.print(list[i] + " ");
		}
		System.out.println();
	}
}
